var pos;
class Dot {
    constructor() {
        this.pos = createVector(canvasWidth / 2, canvasHeight - 10);
        this.vel = createVector(0, 0);
        this.acc = createVector(0, 0);
        this.brain = new Brain(1000);
        this.dead = false;
        this.reachedGoal = false;
        this.isBest = false;
        this.fitness = 0;
        this.showParent = false;
    }

    move() {
        if (this.brain.directions.length > this.brain.step) {
            this.acc = this.brain.directions[this.brain.step];
            this.brain.step++;
        } else {
            this.dead = true;
        }
        this.vel.add(this.acc);
        this.vel.limit(10);
        this.pos.add(this.vel);
    }

    show() {
        if (this.isBest) {
            stroke(255);
            fill(255, 127, 127);
            ellipse(this.pos.x, this.pos.y, 10, 10);
        } else {
            if (!this.showParent) {
                stroke(255);
                fill(0, 127, 255);
                ellipse(this.pos.x, this.pos.y, 4, 4);
            }
        }
    }

    update() {
        if (!this.dead && !this.reachedGoal) {
            this.move();
            if ((this.pos.x > canvasWidth || this.pos.y > canvasHeight) || (this.pos.x < 0 || this.pos.y < 0)) {
                this.dead = true
            } else if (dist(this.pos.x, this.pos.y, goal.x, goal.y) < 5) {
                this.reachedGoal = true;
            } else if (this.pos.x < 600 && this.pos.y < 320 && this.pos.x > 0 && this.pos.y > 300) {
                this.dead = true;
            } else if (this.pos.x > canvasWidth - 600 && this.pos.y < 620 && this.pos.x > 0 && this.pos.y > 600) {
                this.dead = true;
            }
        }
    }

    calculateFitness() {
        if (this.reachedGoal) {
            this.fitness = ((1.0 / 16.0) + 10000.0) / ((float)(this.brain.step * this.brain.step));
        } else {
            let distanceToGoal = dist(this.pos.x, this.pos.y, goal.x, goal.y);
            this.fitness = 1.0 / (distanceToGoal * distanceToGoal);
        }
    }

    getChild() {
        let baby = new Dot();
        baby.brain.directions = [].concat(this.brain.directions);
        return baby;
    }
}
